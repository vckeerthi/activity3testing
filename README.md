# Activity 3
## Group Details
Vivek Batra (C0741344)  
Vidhya Chandrakeerthi (C0741753)  
Pintu Jaat (C0744398)
___
## Activity Description
Mc Donlands Subscribe Feature Test Case. In this activity, we are testing the subscription feature on the website with the following scenarios.  

* Check the title of the "subscribe" tile on the home page.
* Filling in the form with proper information and then subscribing.
* Trying to subscribe without filling in any details on the form.


---
# Required Softwares
For this Activity following Softwares were used:  

* Eclipse
* Selenium
* Chrome Driver

---

# How to Use?
###To run the test case  
1. Simply just download the file and configure the chrome driver in the coding using eclipse
2. Click on Run button in eclipse
3. Wait for Google Chrome to do all its stuff
4. Go back to "JUnit tab" on Eclipse
5. If the bar there is in red color, the test case failed. If it is green all the test cases passed. Hence the expected output is equal to the actual output

---

# Other information

##We have encountered an issue with Captcha prompting while running automated testing and the Captcha's button cant be captured properly. So we have made changes to the Activity.


 
