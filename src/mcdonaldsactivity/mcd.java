package mcdonaldsactivity;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class mcd {

	WebDriver driver;

	@BeforeEach
	void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", 
				"/Users/sm/Desktop/chromedriver");
	    driver = new ChromeDriver();
		
		driver.get("https://www.mcdonalds.com/ca/en-ca.html");

		driver.findElement(By.xpath("//*[@id=\"maincontent\"]/div[2]/div[2]/div/div[1]/div/a")).click();
		
		
	}

	@AfterEach
	void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
		
	}
	

	@Test
	public void testcase1() throws InterruptedException  {
		
		WebElement heading = driver.findElement(By.xpath("//*[@id=\"maincontent\"]/div[1]/div[3]/div[1]/div/div[2]/div/div/div/div/div/div[1]/div[1]/h2"));
		String actualheading = heading.getText();
		assertEquals("Subscribe to My McD’s®",actualheading);
		
	}
	@Test
	public void testcase2() throws InterruptedException{
		WebElement nameBox1 = driver.findElement(By.id("firstname2"));
		nameBox1.sendKeys("chandra keerthi");
		WebElement email1 = driver.findElement(By.id("email2"));
		email1.sendKeys("chandrakeerthi246@gmail.com");
		WebElement postalCode1 = driver.findElement(By.id("postalcode2"));
		postalCode1.sendKeys(" m1t ");
		WebElement showButton1 = driver.findElement(
				By.id("g-recaptcha-btn-2"));
		showButton1.click();
	}
	@Test
	public void testcase3() throws InterruptedException{
		WebElement nameBox2 = driver.findElement(By.id("firstname2"));
		nameBox2.sendKeys("");
		WebElement email2 = driver.findElement(By.id("email2"));
		email2.sendKeys("abc@.com");
		WebElement postalCode2 = driver.findElement(By.id("postalcode2"));
		postalCode2.sendKeys(" t ");
		WebElement showButton2 = driver.findElement(
				By.id("g-recaptcha-btn-2"));
		showButton2.click();

//		WebElement error1 = driver.findElement(By.className("rc-imageselect-incorrect-response"));
//		String actualerror = error1.getText();
//		assertEquals("Please try again",actualerror);
//		//*[@id="rc-imageselect"]/div[2]/div[3]
//		
		
	}

}
